﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFIntroDKNL2021.Model
{
    public class PostGradDbContext : DbContext
    {
        public DbSet<Professor> Professor { get; set; }
        public DbSet<Student> Student { get; set; }
        public DbSet<Qualification> Qualification { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source = MININT-07CHMFP\\SQLEXPRESS; Initial Catalog = PGEFDKNL; Integrated Security = True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Qualification>()
                .HasData(new Qualification { Id = 1, Name = "PhD", 
                    Category = "Research" });
            modelBuilder.Entity<Qualification>()
                .HasData(new Qualification { Id = 2, Name = "Masters", 
                    Category = "Research" });
            modelBuilder.Entity<Qualification>()
                .HasData(new Qualification
                {
                    Id = 3,
                    Name = "Degree",
                    Category = "Practical"
                });
        }
    }
}
