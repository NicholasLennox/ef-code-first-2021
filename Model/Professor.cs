﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFIntroDKNL2021.Model
{
    public class Professor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Subject { get; set; }

        public ICollection<Student> Students { get; set; }
        public ICollection<Qualification> Qualifications { get; set; }
    }
}
