﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFIntroDKNL2021.Model
{
    public class Qualification
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }

        public ICollection<Professor> Professors { get; set; }
    }
}
