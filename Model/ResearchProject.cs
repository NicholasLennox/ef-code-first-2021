﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFIntroDKNL2021.Model
{
    public class ResearchProject
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
    }
}
