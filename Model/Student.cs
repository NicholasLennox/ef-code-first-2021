﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFIntroDKNL2021.Model
{
    public class Student
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Dob { get; set; }

        public int ProfessorId { get; set; }
        public Professor Professor { get; set; }
        public ResearchProject Project { get; set; }
    }
}
