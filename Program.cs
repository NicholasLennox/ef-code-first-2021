﻿
using EFIntroDKNL2021.Model;
using System;
using System.Linq;

namespace EFIntroDKNL2021
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public static void AddStudentWithProf()
        {    
            using (PostGradDbContext context = new PostGradDbContext())
            {
                // Could be passed, or otherwise known
                int profID = 1;
                Student studentExistinProf = new Student()
                {
                    FirstName = "Dewald",
                    LastName = "Els",
                    ProfessorId = profID // Can assign the fk directly
                };
                context.Student.Add(studentExistinProf);
            }
        }

        public static void AddStudentWithNewProf()
        {
            using (PostGradDbContext context = new PostGradDbContext())
            {
                Professor newProf = new Professor()
                {
                    FirstName = "Nicholas", LastName = "Lennox", Subject = "Wishful thinking"
                };
                Student studentNewProf = new Student()
                {
                    FirstName = "Dewald", LastName = "Els", Professor = newProf
                };
                // Will add new prof first, then student with FK to new prof
                context.Student.Add(studentNewProf);
                context.SaveChanges();
            }
        }

        public static void InsertingNamedInstance()
        {
            Professor namedProf = new Professor
            {
                FirstName = "John",
                LastName = "Cleese",
                Subject = "Literature"
            };
            using(PostGradDbContext context = new PostGradDbContext())
            {
                context.Professor.Add(namedProf);
                context.SaveChanges();
            }
        }

        public static void InsertingParamter(Professor paramProf)
        {
            using (PostGradDbContext context = new PostGradDbContext())
            {
                context.Professor.Add(paramProf);
                context.SaveChanges();
            }
        }

        public static void InsertingAnnon()
        {
            using (PostGradDbContext context = new PostGradDbContext())
            {
                context.Professor.Add(new Professor() { 
                    FirstName = "Dylan", 
                    LastName = "Moran", 
                    Subject = "Contmporary Comedy"
                });
                context.SaveChanges();
            }
        }
    }
}
